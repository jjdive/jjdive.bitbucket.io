/* eslint-disable import/extensions */
import Formulas from './calculator.js';

const state = { // Well, sort of
  resultsShown: false,
};

// #region Utils
const qs = selector => document.querySelector(selector); // Got tired of wirting this so much
const intToDisplayPrice = (price) => {
  let priceString = price.toLocaleString(
    'en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    },
  );

  // We want to add a space between $ and the actual price number
  priceString = priceString.replace('$', '$ ');

  return priceString;
};
// #endregion

// #region Results panel animation
const PULL_UP_CLASS = 'pull-up';
const calculator = document.getElementsByClassName('calculator')[0];
const container = document.getElementsByClassName('container')[0];
const resultPanel = document.getElementsByClassName('result-panel')[0];
const resultPanelHeight = +resultPanel.getBoundingClientRect().height;

container.style.setProperty('--results-panel-height', `${resultPanelHeight}px`);

const adjustScroll = () => {
  const offset = resultPanelHeight * 0.56;
  const apply = container.classList.contains(PULL_UP_CLASS);

  document.body.style.setProperty(
    '--body-scroll-offset',
    `${apply ? offset : 0}px`,
  );

  window.scrollTo(0, window.scrollY + (apply ? offset : -offset));
};

const animateResultsPanel = () => {
  resultPanel.classList.remove('empty-values');
  calculator.classList.toggle('show-results');
  container.classList.toggle(PULL_UP_CLASS);
  setTimeout(adjustScroll, 420);
};
// #endregion

// #region Basic error handling
const addMissingClass = wrapperEl => wrapperEl.classList.add('missing-field');
const removeMissingClass = wrapperEl => wrapperEl.classList.remove('missing-field');

const checkMissingFields = (formData) => {
  let fieldMissing = false;
  let element;

  if (formData.annualInsurance === '') {
    element = qs('.insurance-input-wrapper');
    addMissingClass(element);
    fieldMissing = true;
  }

  if (formData.annualTax === '') {
    element = qs('.tax-input-wrapper');
    addMissingClass(element);
    fieldMissing = true;
  }

  if (formData.amount === '') {
    element = qs('.amount-input-wrapper');
    addMissingClass(element);
    fieldMissing = true;
  }

  if (fieldMissing) element.querySelector('input').focus();

  return fieldMissing;
};

const invalidateResult = () => {
  if (state.resultsShown) {
    animateResultsPanel();
    state.resultsShown = false;

    qs('[type=submit]').value = 'RECALCULATE';
    qs('.result-panel').classList.add('empty-values');
  }
};

const bindOnInputHook = (inputElement) => {
  inputElement.addEventListener('input', (event) => {
    removeMissingClass(event.target.parentNode);
    invalidateResult();
  });
};

bindOnInputHook(qs('#amount'));
bindOnInputHook(qs('#annualTax'));
bindOnInputHook(qs('#annualInsurance'));
// #endregion

// #region Slider track fill and value update
document.documentElement.classList.add('js');

const yearsSlider = document.getElementById('yearsSlider');
const rateSlider = document.getElementById('rateSlider');
const yearsInput = document.getElementById('yearsInput');
const rateInput = document.getElementById('rateInput');

yearsSlider.addEventListener('input', () => {
  yearsSlider.style.setProperty('--val', +yearsSlider.value);
  yearsInput.value = +yearsSlider.value;
  invalidateResult();
}, false);
rateSlider.addEventListener('input', () => {
  rateSlider.style.setProperty('--val', +rateSlider.value);
  rateInput.value = +rateSlider.value;
  invalidateResult();
}, false);
// #endregion

// #region Result calculations
const populateResults = ({
  amount,
  annualTax,
  annualInsurance,
  years,
  rate,
}) => {
  const principalAndInterest = Formulas.principalAndInterest(rate, amount, years);
  const monthlyTax = Formulas.tax(annualTax);
  const monthlyInsurance = Formulas.insurance(annualInsurance);
  const totalPayment = Formulas.monthlyPayment(principalAndInterest, annualTax, annualInsurance);

  qs('.principal-interest-output-wrapper span:last-of-type')
    .textContent = intToDisplayPrice(principalAndInterest);

  qs('.tax-output-wrapper span:last-of-type')
    .textContent = intToDisplayPrice(monthlyTax);

  qs('.insurance-output-wrapper span:last-of-type')
    .textContent = intToDisplayPrice(monthlyInsurance);

  qs('.total-output-wrapper span:last-of-type')
    .textContent = intToDisplayPrice(totalPayment);
};
// #endregion

// #region Submit handler
document.getElementsByTagName('form')[0].addEventListener('submit', (event) => {
  event.preventDefault();

  if (state.resultsShown) return;

  const formData = {};

  new window.FormData(event.target).forEach((value, key) => {
    formData[key] = value;
  });

  const missingField = checkMissingFields(formData);

  if (!missingField) {
    qs('[type=submit]').focus();

    setTimeout(() => {
      populateResults(formData);
      animateResultsPanel();
      state.resultsShown = true;
    }, window.innerWidth < 900 ? 300 : 1);
  }
});
// #endregion
