import Calculator from './calculator';


test('Principal and interest', () => {
  const interestRate = 8.0;
  const loanAmount = 100000;
  const yearsOfMortgage = 30;
  const expected = 733.76;

  expect(
    Calculator.principalAndInterest(
      interestRate,
      loanAmount,
      yearsOfMortgage,
    ),
  ).toBe(expected);
});

test('Monthly payment', () => {
  const principalAndInterests = 733.76;
  const annualTax = 1000;
  const annualInsurance = 300;
  const expected = 842.09;

  expect(
    Calculator.monthlyPayment(
      principalAndInterests,
      annualTax,
      annualInsurance,
    ),
  ).toBe(expected);
});
