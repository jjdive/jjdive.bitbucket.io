const round = number => Math.round(number * 1e2) / 1e2;

const tax = annualTax => round(annualTax / 12);

const insurance = annualInsurance => round(annualInsurance / 12);

const monthlyPayment = (principalAndInterest, annualTax, annualInsurance) => (
  round(
    principalAndInterest + tax(annualTax) + insurance(annualInsurance),
  )
);

const principalAndInterest = (annualInterestPercentage, principal, yearsOfMortgage) => {
  // Values shown here are monthly based
  const interestRate = (annualInterestPercentage / 100) / 12;
  const numberOfPeriods = yearsOfMortgage * 12;

  const result = (interestRate * principal) / (1 - ((1 + interestRate) ** (-numberOfPeriods)));

  return round(result);
};

export default {
  tax,
  insurance,
  monthlyPayment,
  principalAndInterest,
};
