# Web Mortgage Calculator

A simple webapp to test some HTML, CSS and JavaScript skills.

---

## Goals

### The project should meet these requirements:
1. ✓ The calculator should match the design (Images are inside the "ref" folder).
2. ✓ The page should be responsive.
3. ✓ Use HTML, JS (ES6 version) and SCSS.
4. ✓ Should work in Chrome (should work without transpilation on v61+).
5. ✓ Do not use any frameworks (CSS / Javascript).
6. ✓ Implement animations on the result panel (Video also in the "ref" folder).
7. ✓ Implement basic error handling as shown in the designs.
8. ✓ Submit the code to a private repository on Github, BitBucket or similar (and here we are).

### Bonus
1. Transpile JS for compatibility with other browsers (Safari/iOS).
2. Use of the Airbnb style guide ✓.
3. Unit tests for critical functions ✓ (Just for math formulas. It would be nice to do some E2E using puppeteer, maybe another day).
4. Use JSDocs 3 pattern for comments.
5. Write a list of bugs or known issues for your implementation if needed.
6. README file with instructions ✓.

### Deliverables
1. All source files (ES6 and SCSS) (The "src" directory can be served to see the working webapp).
2. HTML, JS and CSS files (unminified) (Interesting files are index.html, js/main.js and css/styles.scss).

---

## Instructions

There are 2 versions of the final product:

### ES6

The first one uses ES6 modules (type="module" attribute to link the js file) but it needs the src directory to be served. If npm is available, these commands allow to see the page from a local server:

```
git clone https://jjdive@bitbucket.org/jjdive/jjdive.bitbucket.io.git
cd jjdive.bitbucket.io
npm install
npm start
```

### Transpiled (standalone)

A transpiled version of the JavaScript modules (main.js and calculator.js) is already included in dist-es5 folder. This allows the index.html to be opened directly on the browser without any further steps and also should work on older browsers.

---

## The take away (not that succinct)

### Step 1: Project setup

I spent some time thinking about how to accomplish all of the basic requirements and some of the bonus ones. Particularly thinking about ES6/SASS transpilation + Unit testing and not knowing which bundling scenarios were valid for the challenge, i ended up using:
- node-sass and npm scripts for the styles.
- ES6 modules in the browser (<script type="module"...> as they already work in Chrome without transpiling anything).
- Jest + Babel for unit testing (hence the modules).
- ESLint + AirBnB presets for code styling/linting.

By now, i'm giving up on JSDocs as i've never used that and i might do it at the end (also, i might not).

### Step 2: Math stuff and unit tests

Almost simultaneously with step 1, i wrote down the formulas as a module "calculator.js" and 2 simple unit tests for them using Jest (Awesome testing framework, barely used here xD) in the "calculator.test.js" file. That helped me checking that linting was working and also getting basic babel setup working.

### Step 3: HTML/SCSS mockup

First thing i noticed: the mobile design is based on a 320px wide screen. Standard nowdays is 360px from a css perspective, 720px at 2x devicePixelRatio. So, for the challenge, i just adhered to that. I didn't really like how it looks on a real mobile (again, 360px base), it feels too small so maybe i'll make another branch with this adjustment.

Some comments:
- Is the line below the main title intentionaly touching the right edge of the screen on mobile? I left it that way.
- The label for "interest rate" field should say "annual rate of interest", just an opinion.
- Sliders styling implemented with help from https://css-tricks.com/sliding-nightmare-understanding-range-input/ :).

Things that could be improved so far:
- I don't really know BEM stuff or anything similar so i guess my stylesheets could be cleaner (well, every piece of code could be cleaner).
- Adding to that, i think i'm nesting SASS too much. I found myself not being able to use '+' with reverse nesting properly because of this and how the '&' works.

### Step 4: Result panel animation on mobile

This was fun. The demo video shows that the desired effect has both transitions (on 3 elements) and scrolling.

The scrolling is what caught my attention. Doing it at the same time with the transformations on mobile hardware could have performance issues (even though transforms are hardware accelerated). So i wanted to try something: let's make the whole container move upwards with a transformation too so the visual effect is identical (well, as close as i can get it), it will get cut out on the upper edge of the body, BUT i can adjust that and the scroll position after the animation has completed. I was afraid to get some kind of glitch when doing that adjustment but, to my surprise, it worked realy well. Yyaaayyyy!!! :D.

One issue though, the glitch was just waiting around the corner: When collapsing the results panel again, if the initial scroll position is too close to the bottom of the page, the content will jump a bit. This can be avoided by just adding more content to the page :V, so no big deal.

### Step 5: Implementing the app's logic

Starting with the basic error handling: check that Loan Amount, Annual Tax and Annual Insurance fields are not empty when pressing "Calculate". Then binding the math formulas we implemented before and finally showing the right animations.

Now, that's on mobile. It would be nice to have this UX improvements:

1. Focus the first missing field, if any, when pressing "Calculate" (DONE).
2. Hide results panel on mobile when changing a value or removing one (DONE).

But, we have a different behaviour for desktop. The results panel is always visible so, as per the designs, we need to change the submit button's text to "Recalculate". This looks similar to the item 2 shown above. Let's do that for the next commit (DONE).

So we ended up hiding results panel on mobile whenever a value is changed (including the sliders). On desktop, we change the submit button's text and make the old results less opaque.

### Step 6: Testing and improvements

- Sliders appear to have an outline on mobile (FIXED).
- On mobile, using the on-screen keyboard to submit the form doesn't hide the keyboard. Giving focus to the submit button should fix this (FIXED).
- Also, a little delay between hiding the on-screen keyboard and showing the results wouldn't hurt (DONE).
