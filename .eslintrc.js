module.exports = {
  "extends": [
    "airbnb-base"
  ],
  "env": {
    "jest": true,
  },
  "globals": {
    "window": true,
    "document": true,
  },
};