'use strict';

console.log('Hey there');

(function () {
  'use strict';

  var round = function round(number) {
    return Math.round(number * 1e2) / 1e2;
  };

  var tax = function tax(annualTax) {
    return round(annualTax / 12);
  };

  var insurance = function insurance(annualInsurance) {
    return round(annualInsurance / 12);
  };

  var monthlyPayment = function monthlyPayment(principalAndInterest, annualTax, annualInsurance) {
    return round(principalAndInterest + tax(annualTax) + insurance(annualInsurance));
  };

  var principalAndInterest = function principalAndInterest(annualInterestPercentage, principal, yearsOfMortgage) {
    // Values shown here are monthly based
    var interestRate = annualInterestPercentage / 100 / 12;
    var numberOfPeriods = yearsOfMortgage * 12;

    var result = interestRate * principal / (1 - Math.pow(1 + interestRate, -numberOfPeriods));

    return round(result);
  };

  var Formulas = {
    tax: tax,
    insurance: insurance,
    monthlyPayment: monthlyPayment,
    principalAndInterest: principalAndInterest
  };

  /* eslint-disable import/extensions */

  var state = { // Well, sort of
    resultsShown: false
  };

  // #region Utils
  var qs = function qs(selector) {
    return document.querySelector(selector);
  }; // Got tired of wirting this so much
  var intToDisplayPrice = function intToDisplayPrice(price) {
    var priceString = price.toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    // We want to add a space between $ and the actual price number
    priceString = priceString.replace('$', '$ ');

    return priceString;
  };
  // #endregion

  // #region Results panel animation
  var PULL_UP_CLASS = 'pull-up';
  var calculator = document.getElementsByClassName('calculator')[0];
  var container = document.getElementsByClassName('container')[0];
  var resultPanel = document.getElementsByClassName('result-panel')[0];
  var resultPanelHeight = +resultPanel.getBoundingClientRect().height;

  container.style.setProperty('--results-panel-height', resultPanelHeight + 'px');

  var adjustScroll = function adjustScroll() {
    var offset = resultPanelHeight * 0.56;
    var apply = container.classList.contains(PULL_UP_CLASS);

    document.body.style.setProperty('--body-scroll-offset', (apply ? offset : 0) + 'px');

    window.scrollTo(0, window.scrollY + (apply ? offset : -offset));
  };

  var animateResultsPanel = function animateResultsPanel() {
    resultPanel.classList.remove('empty-values');
    calculator.classList.toggle('show-results');
    container.classList.toggle(PULL_UP_CLASS);
    setTimeout(adjustScroll, 420);
  };
  // #endregion

  // #region Basic error handling
  var addMissingClass = function addMissingClass(wrapperEl) {
    return wrapperEl.classList.add('missing-field');
  };
  var removeMissingClass = function removeMissingClass(wrapperEl) {
    return wrapperEl.classList.remove('missing-field');
  };

  var checkMissingFields = function checkMissingFields(formData) {
    var fieldMissing = false;
    var element = void 0;

    if (formData.annualInsurance === '') {
      element = qs('.insurance-input-wrapper');
      addMissingClass(element);
      fieldMissing = true;
    }

    if (formData.annualTax === '') {
      element = qs('.tax-input-wrapper');
      addMissingClass(element);
      fieldMissing = true;
    }

    if (formData.amount === '') {
      element = qs('.amount-input-wrapper');
      addMissingClass(element);
      fieldMissing = true;
    }

    if (fieldMissing) element.querySelector('input').focus();

    return fieldMissing;
  };

  var invalidateResult = function invalidateResult() {
    if (state.resultsShown) {
      animateResultsPanel();
      state.resultsShown = false;

      qs('[type=submit]').value = 'RECALCULATE';
      qs('.result-panel').classList.add('empty-values');
    }
  };

  var bindOnInputHook = function bindOnInputHook(inputElement) {
    inputElement.addEventListener('input', function (event) {
      removeMissingClass(event.target.parentNode);
      invalidateResult();
    });
  };

  bindOnInputHook(qs('#amount'));
  bindOnInputHook(qs('#annualTax'));
  bindOnInputHook(qs('#annualInsurance'));
  // #endregion

  // #region Slider track fill and value update
  document.documentElement.classList.add('js');

  var yearsSlider = document.getElementById('yearsSlider');
  var rateSlider = document.getElementById('rateSlider');
  var yearsInput = document.getElementById('yearsInput');
  var rateInput = document.getElementById('rateInput');

  yearsSlider.addEventListener('input', function () {
    yearsSlider.style.setProperty('--val', +yearsSlider.value);
    yearsInput.value = +yearsSlider.value;
    invalidateResult();
  }, false);
  rateSlider.addEventListener('input', function () {
    rateSlider.style.setProperty('--val', +rateSlider.value);
    rateInput.value = +rateSlider.value;
    invalidateResult();
  }, false);
  // #endregion

  // #region Result calculations
  var populateResults = function populateResults(_ref) {
    var amount = _ref.amount,
        annualTax = _ref.annualTax,
        annualInsurance = _ref.annualInsurance,
        years = _ref.years,
        rate = _ref.rate;

    var principalAndInterest = Formulas.principalAndInterest(rate, amount, years);
    var monthlyTax = Formulas.tax(annualTax);
    var monthlyInsurance = Formulas.insurance(annualInsurance);
    var totalPayment = Formulas.monthlyPayment(principalAndInterest, annualTax, annualInsurance);

    qs('.principal-interest-output-wrapper span:last-of-type').textContent = intToDisplayPrice(principalAndInterest);

    qs('.tax-output-wrapper span:last-of-type').textContent = intToDisplayPrice(monthlyTax);

    qs('.insurance-output-wrapper span:last-of-type').textContent = intToDisplayPrice(monthlyInsurance);

    qs('.total-output-wrapper span:last-of-type').textContent = intToDisplayPrice(totalPayment);
  };
  // #endregion

  // #region Submit handler
  document.getElementsByTagName('form')[0].addEventListener('submit', function (event) {
    event.preventDefault();

    if (state.resultsShown) return;

    var formData = {};

    new window.FormData(event.target).forEach(function (value, key) {
      formData[key] = value;
    });

    var missingField = checkMissingFields(formData);

    if (!missingField) {
      qs('[type=submit]').focus();

      setTimeout(function () {
        populateResults(formData);
        animateResultsPanel();
        state.resultsShown = true;
      }, window.innerWidth < 900 ? 300 : 1);
    }
  });
  // #endregion
})();
